From 9cea9e351d20d58f447b06baa7bb9a3f5cc40ea4 Mon Sep 17 00:00:00 2001
From: Stefano Rivera <stefano@rivera.za.net>
Date: Mon, 19 Dec 2022 19:56:32 -0400
Subject: [PATCH] Update the Debian Python path detection for setuptools >= 60

Debian now (since Python 3.10.2-6) adds the deb_system scheme to
sysconfig. Newer distutils (such as bundled with setuptools >= 60) adds
fetch schemes from sysconfig, rather than duplicating the sysconfig
schemes statically in distutils.command.install.

This change broke meson's deb_system check.

This patch replaces that mechanism (for newer Debian releases) with
explicit scheme selection, which is far simpler.
But it also retains the old mechanism, for older Debian releases that
require it (Debian <= 11).

Fixes: #8739 (for python module, and makes similar minimal changes to the python3 module)

Fixes: https://bugs.debian.org/1026312

Forwarded: https://github.com/mesonbuild/meson/pull/11211
---
 mesonbuild/modules/python.py | 14 +++++++++++---
 1 file changed, 11 insertions(+), 3 deletions(-)

diff --git a/mesonbuild/modules/python.py b/mesonbuild/modules/python.py
index f74d10e4c..68632af2d 100644
--- a/mesonbuild/modules/python.py
+++ b/mesonbuild/modules/python.py
@@ -363,15 +363,23 @@ def get_distutils_paths(scheme=None, prefix=None):
 # default scheme to a custom one pointing to /usr/local and replacing
 # site-packages with dist-packages.
 # See https://github.com/mesonbuild/meson/issues/8739.
-# XXX: We should be using sysconfig, but Debian only patches distutils.
+# Until version 3.10.2-6, Debian only patched distutils, not sysconfig.
 
 if 'deb_system' in distutils.command.install.INSTALL_SCHEMES:
+    # Debian systems before setuptools-bundled distutils was used by default
     paths = get_distutils_paths(scheme='deb_system')
     install_paths = get_distutils_paths(scheme='deb_system', prefix='')
 else:
-    paths = sysconfig.get_paths()
+    if 'deb_system' in sysconfig.get_scheme_names():
+        # Use Debian's custom deb_system scheme (with our prefix)
+        scheme = 'deb_system'
+    elif sys.version_info >= (3, 10):
+        scheme = sysconfig.get_default_scheme()
+    else:
+        scheme = sysconfig._get_default_scheme()
+    paths = sysconfig.get_paths(scheme=scheme)
     empty_vars = {'base': '', 'platbase': '', 'installed_base': ''}
-    install_paths = sysconfig.get_paths(vars=empty_vars)
+    install_paths = sysconfig.get_paths(vars=empty_vars, scheme=scheme)
 
 def links_against_libpython():
     from distutils.core import Distribution, Extension
-- 
2.35.1

